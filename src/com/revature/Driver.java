package com.revature;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Locale;

public class Driver {

    public static void main(String[] args) {
        try {
            Class clazz = Class.forName("com.revature.models.Customer");
            Field[] fields = clazz.getDeclaredFields();
            System.out.println("Fields found in my class:");
            for(Field f: fields){
                System.out.println(f.getName());
                String fieldName = f.getName();
                String getterName = "get"+ fieldName.substring(0,1).toUpperCase()+fieldName.substring(1);
                System.out.println("this field's getter would be: "+getterName);
            }
            System.out.println("-----------");
            System.out.println("Methods found in my class");
            for(Method m : clazz.getDeclaredMethods()){
                System.out.println(m);
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
