package com.revature;

import com.revature.models.Customer;
import com.revature.util.JsonSerializer;

public class JsonConverterDriver {

    public static void main(String[] args) {
        Customer c = new Customer(34,"Carolyn", "carolyn.rehm@gmail.com");
        JsonSerializer jsonSerializer = new JsonSerializer();
        String serializedCustomer = jsonSerializer.convertObjectToJson(c);
        System.out.println(serializedCustomer);
    }
}
