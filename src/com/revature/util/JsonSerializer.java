package com.revature.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class JsonSerializer {

    public String convertObjectToJson(Object o){
        StringBuilder jsonResult = new StringBuilder("{");

        // obtain the class of the object passed in as an argument
        Class<?> c = o.getClass();

        // get the fields of the class passed in
        Field[] fields = c.getDeclaredFields();

        for(int i=0;i<fields.length; i++){
            // iterate over the fields to get the name of the field and the getter name based on that field
            String fieldName = fields[i].getName();
            String getterName = "get"+ fieldName.substring(0,1).toUpperCase()+fieldName.substring(1);

            try {
                // obtain the method that matches the getter name
                Method getter = c.getMethod(getterName);
                // invoke the getter method to get the value of the field
                Object getterResult = getter.invoke(o);

                // use the field name and the result of the getter method (field value) to generate the JSON string
                jsonResult.append(" \"").append(fieldName).append("\" : \"").append(getterResult).append("\"");

                // append a comma if the field is not the last one
                if(i!=fields.length-1){
                    jsonResult.append(", ");
                }
            } catch (NoSuchMethodException e) {
                System.out.println("your fields do not match your getter methods, check that they are properly" +
                        " formatted");
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                System.out.println("there was an issue invoking your getter -  getter may not be accessible");
            }
        }
        // at the ending bracket and return the JSON string
        return jsonResult.append(" }").toString();
    }
}
